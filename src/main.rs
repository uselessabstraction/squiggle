//  Squiggle - A GTK+ Tildes client programmed in Rust
//  Copyright (C) 2018  Albert James Brown
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![allow(unused_imports)]

extern crate gio;
extern crate gtk;

use gio::prelude::*;
use gtk::prelude::*;

fn main() {
    match gtk::Application::new("com.gitlab.uselessabstraction.squiggle",
                                gio::ApplicationFlags::FLAGS_NONE) {
        Ok(app) => {
            app.connect_activate(|app| activate(app));
            app.run(&[]);
        }
        Err(e) => println!("Error: GTK+ initialization failed: {:?}", e)
    }
}

fn activate(app: &gtk::Application) {
    println!("Activate!");

    let builder = gtk::Builder::new();
    builder.add_from_file("ui/gtk/ui.glade").expect("Error loading Glade UI");

    let window: gtk::ApplicationWindow = builder.get_object("squiggle_window")
        .expect("Error building squiggle_window");

    window.set_application(app);

    window.show();
}

