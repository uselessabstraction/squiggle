//  Squiggle - A GTK+ Tildes client programmed in Rust
//  Copyright (C) 2018  Albert James Brown
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

extern crate chrono;

use self::chrono::{DateTime, Utc};

pub struct Group {
}

pub enum TopicBody {
    Link(String),
    Text(String)
}

pub struct Topic {
    title: String,
    body: TopicBody,
    votes: u32,
    time: DateTime<Utc>,
    editTime: Option<DateTime<Utc>>
}
