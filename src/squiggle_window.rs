//  Squiggle - A GTK+ Tildes client programmed in Rust
//  Copyright (C) 2018  Albert James Brown
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

/*
extern crate gio;
extern crate gtk;

use gio::prelude::*;
use gtk::prelude::*;

fn get_or_die<T: IsA<gtk::Object>>(builder: gtk::Builder, name: &str) -> T {
    builder.get_object(name)
        .expect(format!("Error: Couldn't locate \"{}\" in Glade UI.", &name).as_str())
}

pub struct SquiggleWindow {
    application: gtk::Application,
    window: gtk::ApplicationWindow,
    headerbar_stack: gtk::Stack,
    window_stack: gtk::Stack,
    login_button: gtk::Button
}

impl SquiggleWindow {
    
    pub fn build(app: gtk::Application, builder: gtk::Builder)
                 -> SquiggleWindow {
        let w = SquiggleWindow {
            application: app,
            window: get_or_die(builder, "squiggle_window"),
            headerbar_stack: get_or_die(builder, "headerbar_stack"),
            window_stack: get_or_die(builder, "window_stack"),
            login_button: get_or_die(builder, "login_button")
        };

        w.window.set_application(app);

        w
    }
}
*/
